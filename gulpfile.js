var gulp = require('gulp');
var jshint = require('gulp-jshint');
var browserSync = require('browser-sync');

gulp.task('serve', function() {
  browserSync({
    server: {
      baseDir: 'app',
      routes: {
        '/bower_components': 'bower_components',
        '/test': 'test'
      }
    }
  });
});

gulp.task('lint', function() {
  return gulp.src([
    'app/scripts/**/*.js',
    'test/**/*.js'
  ])
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('js', ['lint'], function() {
  console.log('reload');
  browserSync.reload();
});

gulp.task('default', ['serve', 'lint'], function() {
  gulp.watch([
    'app/*.html',
    'app/scripts/**/*.glsl'
  ], browserSync.reload);
  gulp.watch([
    'app/scripts/**/*.js',
    'test/**/*.js'
  ], ['js']);
});
