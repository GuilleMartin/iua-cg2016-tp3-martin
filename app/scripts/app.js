require(['common'], function() {
  // require(['sample/webgl/00-point/module']);
  // require(['sample/webgl/01-points/module']);
  // require(['sample/webgl/02-colored-points/module']);
  // require(['sample/webgl/03-indexed-colored-points/module']);
  // require(['sample/webgl/04-uniform-colored-points/module']);
  // require(['sample/webgl/05-triangle-and-square/module']);
  // require(['sample/webgl/06-translate-triangle-and-square/module']);
  // require(['sample/webgl/07-textures/module']);
  // require(['sample/webgl/07-textures/two-textures']);
  // require(['sample/webgl/08-dom-events/module']);
  // require(['sample/webgl/09-physics-engine/module']);
  // require(['sample/webgl/10-twgl/module']);
  // require(['sample/webgl/11-twgl-texture/module']);
  // require(['sample/webgl/12-twgl-lights/module']);
  // require(['sample/webgl/13-twgl-picking/module']);
  // require(['sample/webgl/14-twgl-physics/module']);
   require(['tp03_impl/main']);
});


// require(['common'], function() {
//   require([
//     'lib/webgl',
//     'text!shaders/vertex-shader.txt',
//     'text!shaders/fragment-shader.txt'
//   ], function(webgl, vsSource, fsSource) {
//     var gl = webgl.getContext(document.getElementById('canvas'));

//     // Specify the color for clearing <canvas>
//     gl.clearColor(0.0, 0.0, 0.0, 1.0);

//     // Clear <canvas>
//     gl.clear(gl.COLOR_BUFFER_BIT);

//     // load Program
//     var program = webgl.loadProgram(gl, vsSource, fsSource);
//     gl.useProgram(program);

//     // Draw a point
//     gl.drawArrays(gl.POINTS, 0, 1);
//   });

// });

// require(['common'], function() {
//   require(['lib/app']);
// });

// require(['common', 'color/hex'], function(hexColor) {
//   hexColor.init();
//   console.log(hexColor.vec4());
// });

// Module definition
// http://requirejs.org/docs/api.html#define
// require(['sample/require/simple'], function(simple) {
// require(['sample/require/function-definition'], function(simple) {
// require(['sample/require/function-definition-with-deps'], function(simple) {
//  console.log(simple);
// });

// Module with text
// Notice that "common" needs to be loaded first
// require(['common', 'sample/text/module-using-text'], function(common, str) {
//   console.log(str);
// });


// Module that loads dat-gui
// Notice that "common" loads module definitions
// require(['common'], function() {
//   // With modules definitions "dat" can be loaded
//   require(['dat'], function(dat) {
//     console.log(dat);
//   });
// });
