define(function() {
  return {
    /**
     * Transforma un array de arrays en un array
     * Por ejemplo
     *   > flatten([[1,2], [3,4]]);
     *   < [1,2,3,4]
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/
     * Reference/Global_Objects/Array/Reduce
     *
     * @param  {Array<Array<T>>} xs Array de arrays de tipo T
     * @return {Array<T>}
     */
    flatten: function(xs) {
      return xs.reduce(function(a, b) {
        return a.concat(b);
      });
    }
  };
});
