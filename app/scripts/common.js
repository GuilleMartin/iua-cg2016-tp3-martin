require.config({
  baseUrl: '/scripts',
  paths: {
    jquery: '../bower_components/jquery/dist/jquery',
    glMatrix: '../bower_components/gl-matrix/dist/gl-matrix',
    text: '../bower_components/requirejs-text/text',
    dat: '../bower_components/dat-gui/build/dat.gui',
    WebGLUtils: 'vendor/webgl-utils',
    WebGLDebugUtils: 'vendor/webgl-debug',
    CuonUtils: 'vendor/cuon-utils',
	twgl: 'vendor/twgl'
  },
  shim: {
    dat: {
      exports: 'dat'
    },
    WebGLUtils: {
      exports: 'WebGLUtils'
    },
    WebGLDebugUtils: {
      exports: 'WebGLDebugUtils'
    },
    CuonUtils: {
      deps: ['WebGLUtils', 'WebGLDebugUtils'],
      exports: 'CuonUtils'
    },
	twgl:{
		exports: 'twgl'
	}
  }
});
