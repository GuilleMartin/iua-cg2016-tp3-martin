define(['./geometry'], function(Geometry){

  var SphereGeometry = Object.create(Geometry);

  SphereGeometry.init = function(edges, radius){
    Geometry.init.call(this);
    this._radius = radius;
    this._edges = edges;

    var i, ai, si, ci;
    var j, aj, sj, cj;
    var p1, p2;

    //Vertex & normals
    for (j = 0; j <= this._edges; j++) {
      aj = j * Math.PI / this._edges;
      sj = Math.sin(aj) * this._radius;
      cj = Math.cos(aj) * this._radius;

      for (i = 0; i <= this._edges; i++) {
        ai = i * 2 * Math.PI / this._edges;
        si = Math.sin(ai);
        ci = Math.cos(ai);
        this.vertices.push([(si * sj), (cj), (ci * sj)]);

        //Normals:
        this.normals.push([(si * sj), (cj), (ci * sj)]);
      }
    }

    //Faces
    for (j = 0; j < this._edges; j++) {
      for (i = 0; i < this._edges; i++) {
        p1 = j * (this._edges+1) + i;
        p2 = p1 + (this._edges+1);
        this.faces.push([p1, p2, (p1 + 1)]);
        this.faces.push([(p1 + 1), p2, (p2 + 1)]);
      }
    }

    //Textures coordenates
    for (j = this._edges; j >=0 ; j--) {
      for (i = this._edges; i >=0; i--) {
        this.st.push( [1-(i/this._edges) , j/this._edges]);
      }
    }
  };

  Object.defineProperty(SphereGeometry, 'radius', {
    get: function() {
      return this._radius;
    },
    set: function(value) {
      this._radius = value;
    }
  });

  Object.defineProperty(SphereGeometry, 'edges', {
    get: function() {
      return this._edges;
    },
    set: function(value) {
      this._edges = value;
    }
  });

  return SphereGeometry;
});
