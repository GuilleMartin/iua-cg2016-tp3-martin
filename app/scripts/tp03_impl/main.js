define(['glMatrix',
        'dat',
        './constants',
        './config',
        './webTWGLRenderer',
        './scene',
        './mesh',
        './geometry',
        './material',
        './regularConvexPolygonGeometry',
        './sphereGeometry',
        './cubeGeometry',
        './cylinderGeometry',
        './orthographicCamera',
        './perspectiveCamera',
        './light',
        './pointLight',
        './axes'],
        function(glMatrix,
                 dat,
                 Constants,
                 Config,
                 WebGLRenderer,
                 Scene,
                 Mesh,
                 Geometry,
                 Material,
                 Polygon,
                 Sphere,
                 Cube,
                 Cylinder,
                 OrthographicCamera,
                 PerspectiveCamera,
                 Light,
                 PointLight,
                 Axes){

  var cons = Object.create(Constants);
  var colors = cons.colors;
  var geometryType = cons.geometryType;

  var i;
  var config = Object.create(Config);
  config.init();

  var gui = new dat.GUI();
  var canvas = document.getElementById('canvas');

  var scene = Object.create(Scene);

  //Seteo el color de fondo, luz ambiental y puntual
  scene.init(config.clearColor);

  //AmbientLight:
  var ambientLight = Object.create(Light);
  ambientLight.init(config.paramAmbientLight);
  scene.ambientLight = ambientLight;

  //Ambient color menu handler.
  var ambientLightFolder = gui.addFolder('Ambient Light');
  var ambientController = ambientLightFolder.addColor(ambientLight, 'hexColor');
  ambientController.onChange(function(value){
    ambientLight.init(value);
  });

  //PointLight: setup from config and set the color controller
  var pointLight = null;
  var pointLightFolder = null;
  var pointLightController = [];

  for(i=0; i<5; i++){
    pointLight = Object.create(PointLight);
    pointLight.init(config.paramPointLight[i].px,
                    config.paramPointLight[i].py,
                    config.paramPointLight[i].pz,
                    config.paramPointLight[i].diffuse,
                    config.paramPointLight[i].enabled);



    var visible = config.paramPointLight[i].enabled;
    var paramPointLightMesh = {
                             type: geometryType.SPHERE,
                             attrib: {
                                      radius:       0.25,
                                      edges:        20,
                                      height:       null,
                                      tx:           config.paramPointLight[i].px,
                                      ty:           config.paramPointLight[i].py,
                                      tz:           config.paramPointLight[i].pz,
                                      useLighting:  false,
                                      useTexture:   false,
                                      pickingColor: colors.BLACK,
                                      visible:      visible
                                     },
                             material: {
                                        diffuse:   config.paramPointLight[i].diffuse,
                                        specular:  [0.7, 0.6, 0.6],
                                        shininess: 0.25 * 20,
                                        texture:   ''
                                       }
                            };

    var pointLightMesh = generateMesh(paramPointLightMesh);
    scene.addMesh(pointLightMesh);
    pointLight.pointLightMeshId = scene.lastMeshId;

    scene.pointLights.push(pointLight);

    pointLightFolder = gui.addFolder('Point Light ' + i);
    pointLightFolder.add(pointLight,'px',-10,10);
    pointLightFolder.add(pointLight,'py',-10,10);
    pointLightFolder.add(pointLight,'pz',-10,10);
    pointLightFolder.add(pointLight,'enabled');
    pointLightController.push(pointLightFolder.addColor(scene.pointLights[0], 'hexColor'));
  }

  pointLightController[0].onChange(function(value){
    scene.pointLights[0].diffuse = value;
  });

  pointLightController[1].onChange(function(value){
    scene.pointLights[1].diffuse = value;
  });

  pointLightController[2].onChange(function(value){
    scene.pointLights[2].diffuse = value;
  });

  pointLightController[3].onChange(function(value){
    scene.pointLights[3].diffuse = value;
  });

  pointLightController[4].onChange(function(value){
    scene.pointLights[4].diffuse = value;
  });

  //Obtengo los meshes de la grilla
  var grid = getGrid();

  if(grid !== undefined){
    for(i = 0; i < grid.length; i++){
      scene.addMesh(grid[i]);
    }
  }

  //Obtengo los meshes de los ejes X, Y, Z
  var ax = Object.create(Axes);
  ax.init(0, 0, 0);

  var axes = ax.getAxes();

  if(axes !== undefined){
    for(i = 0; i < axes.length; i++){
      scene.addMesh(axes[i]);
    }
  }

  var mesh = null;
  var meshFolder;

  //Se instancian los meshes desde la configuración
  for(i=0; i < config.paramMesh.length; i++){
    mesh = generateMesh(config.paramMesh[i]);
    scene.addMesh(mesh);

    //GUI Mesh i
    meshFolder = gui.addFolder('Mesh-' + (i + 1) + (' (' + mesh.name + ')'));
    meshFolder.add(mesh, 'tx', -10, 10, 0.01);
    meshFolder.add(mesh, 'ty', -10, 10, 0.01);
    meshFolder.add(mesh, 'tz', -10, 10, 0.01);
    meshFolder.add(mesh, 'rx', -10, 10);
    meshFolder.add(mesh, 'ry', -10, 10);
    meshFolder.add(mesh, 'rz', -10, 10);
    meshFolder.add(mesh, 'sx', -10, 10);
    meshFolder.add(mesh, 'sy', -10, 10);
    meshFolder.add(mesh, 'sz', -10, 10);
    meshFolder.add(mesh, 'useTexture');
    meshFolder.add(mesh, 'useLighting');
  }

  //Camera
  var camera;
  if(config.paramCamera === cons.cameraType.ORTHOGRAPHIC){
    camera = Object.create(OrthographicCamera);
  }else{
    camera = Object.create(PerspectiveCamera);
  }
  camera.init();

  //GUI Camera
  var cameraFolder = gui.addFolder('Camera');
  cameraFolder.add(camera, 'centerX', -10, 10, 0.01);
  cameraFolder.add(camera, 'centerY', -10, 10, 0.01);
  cameraFolder.add(camera, 'centerZ', -10, 10, 0.01);
  cameraFolder.add(camera, 'eyeX', -10, 10, 0.01);
  cameraFolder.add(camera, 'eyeY', -10, 10, 0.01);
  cameraFolder.add(camera, 'eyeZ', -10, 10, 0.01);
  cameraFolder.add(camera, 'upX', -10, 10, 0.01);
  cameraFolder.add(camera, 'upY', -10, 10, 0.01);
  cameraFolder.add(camera, 'upZ', -10, 10, 0.01);

  if(config.paramCamera === cons.cameraType.ORTHOGRAPHIC){
    cameraFolder.add(camera, 'right', -10, 10, 0.01);
    cameraFolder.add(camera, 'left', -10, 10, 0.01);
    cameraFolder.add(camera, 'top', -10, 10, 0.01);
    cameraFolder.add(camera, 'bottom', -10, 10, 0.01);
  }else{
    cameraFolder.add(camera, 'fovy', 0, Math.PI, 0.01);
    cameraFolder.add(camera, 'aspect', 0.01, 5, 0.01);
  }

  cameraFolder.add(camera, 'near', 0, 100, 0.01);
  cameraFolder.add(camera, 'far', 1000, 10000, 0.01);

  var renderer = Object.create(WebGLRenderer);
  renderer.init(canvas, scene, camera, config.drawMode);

  gui.add(renderer, 'drawMode', { LINE_LOOP: 0, TRIANGLES: 1} );

  var lastTime = 0;

  function tick(currentTime){

    var deltaTime = currentTime - lastTime;
    lastTime = currentTime;

    updatePointLighMesh(scene);
    renderer.render(scene, camera, deltaTime);
    requestAnimationFrame(tick);
  }

  tick(lastTime);

  function getGrid(){
    var grid = [];
    var i;
    var lines;
    var gridMesh;
    var gridNum = config.gridNum;

    var material = Object.create(Material);
    material.init('#808080', [0, 0, 0], 0, '');

    //Generate the Line Grid Meshes:
    for(i=-gridNum; i <=gridNum; i++){

      lines = Object.create(Geometry);
      lines.init();
      lines.vertices.push([-gridNum, 0, i], [gridNum, 0, i]);
      lines.faces.push([1, 0]);
      lines.normals.push([-gridNum, 0, i], [gridNum, 0, i]);
      lines.st.push([1,1]);

      gridMesh = Object.create(Mesh);
      gridMesh.init(lines, material, 'LINES');
      gridMesh.useLighting = false;
      gridMesh.useTexture = false;
      gridMesh.pickingColor = colors.BLACK;
      grid.push(gridMesh);
    }

    for(i=-gridNum; i <= gridNum; i++){

      lines = Object.create(Geometry);
      lines.init();
      lines.vertices.push([i, 0, -gridNum], [i, 0, gridNum]);
      lines.faces.push([1, 0]);
      lines.normals.push([-gridNum, 0, i], [gridNum, 0, i]);
      lines.st.push([1,1]);

      gridMesh = Object.create(Mesh);
      gridMesh.init(lines, material, 'LINES');
      gridMesh.useLighting = false;
      gridMesh.useTexture = false;
      gridMesh.pickingColor = colors.BLACK;
      grid.push(gridMesh);
    }

    return grid;
  }

  function generateMesh(params){
    var geometry = null;
    var material = null;
    mesh = Object.create(Mesh);

    if(params.type === geometryType.SPHERE){
      geometry = generateSphere(params);
      mesh.name = 'Sphere';
    }else if(params.type === geometryType.CUBE){
      geometry = generateCube(params);
      mesh.name = 'Cube';
    }else if(params.type === geometryType.CYLINDER){
      geometry = generateCylinder(params);
      mesh.name = 'Cylinder';
    }

    material = Object.create(Material);
    material.init(params.material.diffuse,
                  params.material.specular,
                  params.material.shininess,
                  params.material.texture);

    mesh.init(geometry, material, 'TRIANGLES');

    mesh.tx = params.attrib.tx;
    mesh.ty = params.attrib.ty;
    mesh.tz = params.attrib.tz;
    mesh.useLighting = params.attrib.useLighting;
    mesh.useTexture = params.attrib.useTexture;
    mesh.pickingColor = params.attrib.pickingColor;
    mesh.visible = params.attrib.visible;

    return mesh;
  }

  function generateSphere(params){
    var sphere = Object.create(Sphere);
    sphere.init(params.attrib.edges,
                params.attrib.radius);

    return sphere;
  }

  function generateCube(params){
    var cube = Object.create(Cube);
    cube.init(params.attrib.height);
    return cube;
  }

  function generateCylinder(params){
    var cylinder = Object.create(Cylinder);
    cylinder.init(params.attrib.radius,
                  params.attrib.height,
                  params.attrib.edges);
    return cylinder;
  }

  function updatePointLighMesh(scene){
    for(var i=0; i<5; i++){
      scene.meshes[scene.pointLights[i].pointLightMeshId].visible = scene.pointLights[i].enabled;
      scene.meshes[scene.pointLights[i].pointLightMeshId].tx = scene.pointLights[i].px;
      scene.meshes[scene.pointLights[i].pointLightMeshId].ty = scene.pointLights[i].py;
      scene.meshes[scene.pointLights[i].pointLightMeshId].tz = scene.pointLights[i].pz;
      scene.meshes[scene.pointLights[i].pointLightMeshId].material.diffuse = scene.pointLights[i].diffuse;
    }
  }
});
