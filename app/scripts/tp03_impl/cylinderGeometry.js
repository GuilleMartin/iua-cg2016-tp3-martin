define(['./geometry'], function(Geometry){

  var CylinderGeometry = Object.create(Geometry);

  CylinderGeometry.init = function(radius, height, edges){

    Geometry.init.call(this);
    this._radius = radius;
    this._height = height;
    this._edges = edges;

    var i;
    var angle;
    var vertex = 0;
    var x, z, u;

    //Side Vertex & normals
    for (i=0; i <= this._edges ; i++){
      angle = (i * 2 * Math.PI) / this._edges;

      x = Math.cos(angle) * this.radius;
      z = Math.sin(angle) * this.radius;

      this._vertices.push([x, height/2, z]); //(x,y,z)
      this._vertices.push([x, -height/2, z]); //(x,y,z)
      vertex += 2;

      this._normals.push([x, 0, z], [x, 0, z]);

      u = i/this._edges;
      this.st.push([u, 1.0], [u, 0.0]);
    }

    //Side faces
    for (i=0; i < this._edges * 2; i+=2){
      this._faces.push([i+1, i, i+2], [i+1, i+2, i+3]);
    }

    //Top vertex, face and normals
    this._vertices.push([0, height/2, 0]);
    this._normals.push([0, 1, 0]);

    for (i=vertex; i <= (this._edges + vertex) ; i++){
      angle = (i * 2 * Math.PI) / this._edges;

      x = Math.cos(angle) * this.radius;
      z = Math.sin(angle) * this.radius;

      this._vertices.push([x, height/2, z]); //(x,y,z)
      this.faces.push([vertex, i+1, i]);
      this._normals.push([0, 1, 0]);

      u = i/this._edges;
      this.st.push([u, 1.0], [u, 0.0]);
    }
    vertex += this._edges + 1;

   //Bottom vertex, face and normals
    this._vertices.push([0, -height/2, 0]);
    this._normals.push([0, -1, 0]);
    vertex++;

    for (i=vertex; i <= (this._edges + vertex) ; i++){
      angle = (i * 2 * Math.PI) / this._edges;

      x = Math.cos(angle) * this.radius;
      z = Math.sin(angle) * this.radius;

      this._vertices.push([x, -height/2, z]); //(x,y,z)
      this.faces.push([vertex, i, i+1]);
      this._normals.push([0, -1, 0]);

      u = i/this._edges;
      this.st.push([u, 1.0], [u, 0.0]);
    }
  };

  Object.defineProperty(CylinderGeometry, 'radius', {
    get: function() {
      return this._radius;
    },
    set: function(value) {
      this._radius = value;
    }
  });

  Object.defineProperty(CylinderGeometry, 'height', {
    get: function() {
      return this._height;
    },
    set: function(value) {
      this._height = value;
    }
  });

  Object.defineProperty(CylinderGeometry, 'edges', {
    get: function() {
      return this._edges;
    },
    set: function(value) {
      this._edges = value;
    }
  });

  Object.defineProperty(CylinderGeometry, 'vertices', {
    get: function() {
      return this._vertices;
    },
    set: function(value) {
      this._vertices = value;
    }
  });

  Object.defineProperty(CylinderGeometry, 'faces', {
    get: function() {
      return this._faces;
    },
    set: function(value) {
      this._faces = value;
    }
  });

  Object.defineProperty(CylinderGeometry, 'normals', {
    get: function() {
      return this._normals;
    },
    set: function(value) {
      this._normals = value;
    }
  });

  return CylinderGeometry;
});
