define(function(){

  var Constants = {
    colors: {
      RED:       [255,0,0,255],
      GREEN:     [0,255,0,255],
      BLUE:      [0,0,255,255],
      YELLOW :   [1,1,0,1],
      LIGHTBLUE: [0,1,1,1],
      PURPLE:    [1,0,1,1],
      BLACK:     [0,0,0,1],
      WHITE:     [0.5,0.5,0.5,1]
    },

    cameraType: {
      PERSPECTIVE:  0,
      ORTHOGRAPHIC: 1
    },

    drawModes: {
      LINE_LOOP: 0,
      TRIANGLES: 1
    },

    geometryType: {
      SPHERE:   0,
      CUBE:     1,
      CYLINDER: 2
    }
  };

  return Constants;
});
