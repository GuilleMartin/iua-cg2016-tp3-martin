define(['glMatrix','./regularConvexPolygonGeometry'], function(glMatrix, RegularConvexPolygonGeometry) {

  var pc = Object.create(RegularConvexPolygonGeometry);
  pc.init();

  return pc;
});
