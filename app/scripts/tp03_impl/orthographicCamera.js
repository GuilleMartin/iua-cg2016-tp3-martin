define(['./camera', 'glMatrix'], function(Camera, glMatrix) {

  var OrthographicCamera = Object.create(Camera);

  OrthographicCamera.init = function(left, right, bottom, top, near, far){

    Camera.init.call(this);

    if(left === undefined){
      this._left = -10;
    }else{
      this._left = left;
    }

    if(right === undefined){
      this._right = 10;
    }else{
      this._right = right;
    }

    if(bottom === undefined){
      this._bottom = -10;
    }else{
      this._bottom = bottom;
    }

    if(top === undefined){
      this._top = 10;
    }else{
      this._top = top;
    }

    if(near === undefined){
      this._near = 0.001;
    }else{
      this._near = near;
    }

    if(far === undefined){
      this._far = 1000;
    }else{
      this._far = far;
    }
  };

  Object.defineProperty(OrthographicCamera, 'right', {
    get: function() {
      return this._right;
    },
    set: function(value) {
      this._right = value;
    }
  });

  Object.defineProperty(OrthographicCamera, 'left', {
    get: function() {
      return this._left;
    },
    set: function(value) {
      this._left = value;
    }
  });

  Object.defineProperty(OrthographicCamera, 'bottom', {
    get: function() {
      return this._bottom;
    },
    set: function(value) {
      this._bottom = value;
    }
  });

  Object.defineProperty(OrthographicCamera, 'top', {
    get: function() {
      return this._top;
    },
    set: function(value) {
      this._top = value;
    }
  });

  Object.defineProperty(OrthographicCamera, 'near', {
    get: function() {
      return this._near;
    },
    set: function(value) {
      this._near = value;
    }
  });

  Object.defineProperty(OrthographicCamera, 'far', {
    get: function() {
      return this._far;
    },
    set: function(value) {
      this._far = value;
    }
  });

  OrthographicCamera.getProjectionMatrix = function(){
     glMatrix.mat4.ortho(this._projectionMatrix,
                         this._left,
                         this._right,
                         this._bottom,
                         this._top,
                         this._near,
                         this._far);

     return this._projectionMatrix;
  };

  return OrthographicCamera;
});
