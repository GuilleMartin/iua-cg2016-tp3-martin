define(['./constants'], function(Constants){

  var cons = Object.create(Constants);
  var colors = cons.colors;
  var geometryType = cons.geometryType;

  var Config = {

    get gridNum() {
      return this._gridNum;
    },

    get paramMesh() {
      return this._paramMesh;
    },

    get paramPointLight() {
      return this._paramPointLight;
    },

    get paramAmbientLight() {
      return this._paramAmbientLight;
    },

    get clearColor() {
      return this._clearColor;
    },

    get drawMode() {
      return this._drawMode;
    },

    init: function(){
      this._paramMesh = [];
      this._paramPointLight = [];
      this._paramAmbientLight = '#808080';

      //La longitud del vector representa la cantidad de meshes a dibujar
      //Cada elemento del vector paramMesh contiene informacion de la cantidad de lados, color y posición inicial.
      //Subvector:
      //Primer elemento: Cantidad de lados del poligono (edges);
      //Segundo elemento: Color (material).
      //Tercer elemento: Posición inicial (tx, ty, tz).
      this._paramMesh.push(
                            {
                             type: geometryType.SPHERE,
                             attrib: {
                                      radius:       1,
                                      edges:        20,
                                      height:       null,
                                      tx:           -3,
                                      ty:           0,
                                      tz:           0,
                                      useLighting:  true,
                                      useTexture:   false,
                                      pickingColor: colors.BLUE,
                                      visible:      true
                                     },

                             material: {
                                        diffuse:   '#ffffff',
                                        specular:  '#ffffff',
                                        shininess: 0.25 * 20,
                                        texture:   './scripts/tp03_impl/textures/earthmap.jpg'
                                       }
                            },
                            {
                             type: geometryType.CUBE,
                             attrib: {
                                      radius:       null,
                                      edges:        null,
                                      height:       1.5,
                                      tx:           0,
                                      ty:           0,
                                      tz:           0,
                                      useLighting:  true,
                                      useTexture:   false,
                                      pickingColor: colors.RED,
                                      visible:      true
                                     },

                             material: {
                                        diffuse:   '#ffffff',
                                        specular:  '#ffffff',
                                        shininess: 0.25 * 20,
                                        texture:   './scripts/tp03_impl/textures/firefox.png'
                                       }
                            },
                            {
                             type: geometryType.CYLINDER,
                             attrib: {
                                      radius:       0.75,
                                      edges:        20,
                                      height:       1.5,
                                      tx:           3,
                                      ty:           0,
                                      tz:           0,
                                      useLighting:  true,
                                      useTexture:   false,
                                      pickingColor: colors.GREEN,
                                      visible:      true
                                     },

                             material: {
                                        diffuse:   '#ffffff',
                                        specular:  '#ffffff',
                                        shininess: 0.25 * 20,
                                        texture:   './scripts/tp03_impl/textures/barrel.jpg'
                                       }
                            }
                          );

      //Point Lights (MAX: 5)
      this._paramPointLight.push(
                                  {
                                    enabled: true,
                                    px: 0,
                                    py: 5,
                                    pz: 0,
                                    diffuse: '#ffffff'
                                  },
                                  {
                                    enabled: false,
                                    px: -5,
                                    py: -5,
                                    pz: 0,
                                    diffuse: '#ffffff'
                                  },
                                  {
                                    enabled: false,
                                    px: 5,
                                    py: 5,
                                    pz: 0,
                                    diffuse: '#ffffff'
                                  },
                                  {
                                    enabled: false,
                                    px: 0,
                                    py: 0,
                                    pz: -5,
                                    diffuse: '#ffffff'
                                  },
                                  {
                                    enabled: false,
                                    px: 0,
                                    py: 0,
                                    pz: 5,
                                    diffuse: '#ffffff'
                                  }
                                 );

      //tipo de vista
      this.paramCamera = cons.cameraType.PERSPECTIVE;

      //cantidad de grillas
      this._gridNum = 10;

      //Background color
      this._clearColor = cons.colors.BLACK;

      //Draw mode(TRIANGLES, LINE_LOOP)
      this._drawMode = cons.drawModes.TRIANGLES;
    }
  };

  return Config;
});
