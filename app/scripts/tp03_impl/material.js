define(['./color/style'], function(Style){

  var Material = {

    init: function(diffuse, specular, shininess, map){

      this.hexDiffuse = diffuse;
      this._diffuse = Object.create(Style);
      this._diffuse.init(this.hexDiffuse);

      this.hexSpecular = specular;
      this._specular = Object.create(Style);
      this._specular.init(this.hexSpecular);

      this._shininess = shininess;
      this._map = map;
      this._texture = null;
    },

    get diffuse() {
      return this._diffuse;
    },

    set diffuse(value){
      this._diffuse = Object.create(Style);
      this._diffuse.init(value);
    },

    get specular() {
      return this._specular;
    },

    set specular(value){
      this._specular = Object.create(Style);
      this._specular.init(value);
    },

    get shininess() {
      return this._shininess;
    },

    set shininess(value){
      this._shininess = value;
    },

    get map() {
      return this._map;
    },

    set map(value){
      this._map = value;
    },

    get texture() {
      return this._texture;
    },

    set texture(value){
      this._texture = value;
    },
  };

  return Material;
});

