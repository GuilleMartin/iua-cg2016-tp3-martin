define(['./hex'],function(Hex) {

	var Style = Object.create(Hex);

  Style.init = function(style){

		this.style = style;
		var hexColor;

    if (/^\#([0-9a-f]{6})$/i.test(this.style)){ // #ff0000

			hexColor = /^\#([0-9a-f]{6})$/i.exec(this.style);
			this.setHex = (parseInt(hexColor[1], 16));

		}else if ( /^\#([0-9a-f])([0-9a-f])([0-9a-f])$/i.test(this.style)){  // #f00

			hexColor = /^\#([0-9a-f])([0-9a-f])([0-9a-f])$/i.exec(this.style);
			this.setHex = (parseInt(hexColor[1] + hexColor[1] + hexColor[2] + hexColor[2] + hexColor[3] + hexColor[3], 16));

		}else{
			this.setHex = '#ffffff';
		}

    Hex.init.call(this, this.setHex);
	};

	return Style;

});
