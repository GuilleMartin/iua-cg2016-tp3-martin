define(['./rgba'], function(RGBA) {

  var RGB = Object.create(RGBA);

  RGB.init = function(r, g, b){
    RGBA.init.call(this, r, g, b);
  };

  return RGB;
});
