define(['glMatrix'], function(glMatrix) {
//define( function() {
  var RGBA = {

    init: function(r, g, b, a){

      if(r !== undefined){
        this.r = r ;
      }else{
        this.r = 1;
      }

      if(g !== undefined){
        this.g = g;
      }else{
        this.g = 1;
      }

      if(b !== undefined){
        this.b = b;
      }else{
        this.b = 1;
      }

      if(a !== undefined){
        this.a = a;
      }else{
        this.a = 1;
      }
    },

    vec3: function(){
      var vec = glMatrix.vec3.create();

      vec[0] = this.r;
      vec[1] = this.g;
      vec[2] = this.b;

      return vec;
    },

    vec4: function(){
      var vec = glMatrix.vec4.create();

      vec[0] = this.r;
      vec[1] = this.g;
      vec[2] = this.b;
      vec[3] = this.a;

      return vec;
    }
  };

  return RGBA;
});
