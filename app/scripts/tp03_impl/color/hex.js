define(['./rgb'], function(RGB) {

  var Hex = Object.create(RGB);

  Hex.init = function(hexNumber){

    var hex = Math.floor(hexNumber);

    var r = ( hex >> 16 & 255 ) / 255;
    var g = ( hex >> 8 & 255 ) / 255;
    var b = ( hex & 255 ) / 255;

    RGB.init.call(this, r, g, b);

    this.hexNumber = hexNumber;
  };


  return Hex;

});
