define(['./rgba'], function(RGBA) {

  var HSL = Object.create(RGBA);

  function hue2rgb ( p, q, t ) {

        if ( t < 0 ){t += 1;}
        if ( t > 1 ){t -= 1;}
        if ( t < 1 / 6 ){ return p + ( q - p ) * 6 * t;}
        if ( t < 1 / 2 ){ return q;}
        if ( t < 2 / 3 ){ return p + ( q - p ) * 6 * ( 2 / 3 - t );}

        return p;
      }

  HSL.init = function(h, s, l){

    var r, g, b;

    if(s === 0){

      r = g = b = l;

    }else{

      var p = l <= 0.5 ? l * ( 1 + s ) : l + s - ( l * s );
      var q = ( 2 * l ) - p;

      r = hue2rgb( q, p, h/360 + (1 / 3));
      g = hue2rgb( q, p, h/360 );
      b = hue2rgb( q, p, h/360 - (1 / 3));

      }

    RGBA.init.call(this, r, g, b);

    this.h = h;
    this.s = s;
    this.l = l;
  };

  return HSL;
});
