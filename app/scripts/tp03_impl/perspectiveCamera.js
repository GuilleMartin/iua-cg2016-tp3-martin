define(['./camera', 'glMatrix'], function(Camera, glMatrix) {

  var PerspectiveCamera = Object.create(Camera);

  PerspectiveCamera.init = function(fovy, aspect, near, far){

    Camera.init.call(this);

    if(fovy === undefined){
      this._fovy = Math.PI / 4;
    }else{
      this._fovy = fovy;
    }

    if(aspect === undefined){
      this._aspect = 1;
    }else{
      this._aspect = aspect;
    }

    if(near === undefined){
      this._near = 0.001;
    }else{
      this._near = near;
    }

    if(far === undefined){
      this._far = 1000;
    }else{
      this._far = far;
    }
  };

  Object.defineProperty(PerspectiveCamera, 'fovy', {
    get: function() {
      return this._fovy;
    },
    set: function(value) {
      this._fovy = value;
    }
  });

  Object.defineProperty(PerspectiveCamera, 'aspect', {
    get: function() {
      return this._aspect;
    },
    set: function(value) {
      this._aspect = value;
    }
  });

  Object.defineProperty(PerspectiveCamera, 'near', {
    get: function() {
      return this._near;
    },
    set: function(value) {
      this._near = value;
    }
  });

  Object.defineProperty(PerspectiveCamera, 'far', {
    get: function() {
      return this._far;
    },
    set: function(value) {
      this._far = value;
    }
  });

  PerspectiveCamera.getProjectionMatrix = function(){
     glMatrix.mat4.perspective(this._projectionMatrix,
                               this._fovy,
                               this._aspect,
                               this._near,
                               this._far);

     return this._projectionMatrix;
  };

  return PerspectiveCamera;
});
