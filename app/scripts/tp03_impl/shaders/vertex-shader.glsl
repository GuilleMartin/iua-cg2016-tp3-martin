#define NR_POINT_LIGHTS 5

struct PointLight {
  vec3 uPointLightColor;
  vec3 uPointLightPosition;
  bool uPointLightEnabled;
};

attribute vec4 aPosition;
attribute vec4 aNormal;
attribute vec2 aTexcoord;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;
uniform mat4 uNormalMatrix;
uniform vec4 uMaterialDiffuse;
uniform vec3 uAmbientLightColor;
uniform bool uUseLighting;
uniform bool uUseTexture;
uniform PointLight uPointLight[NR_POINT_LIGHTS];

varying vec4 vColor;
varying vec2 vTexcoord;

void main() {
  gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * aPosition;

  // gl.uniform1i(program.uUseLighting, true);
  if (uUseLighting) {
    vec3 normal = normalize(vec3(uNormalMatrix * aNormal));
    vec4 vertexPosition = uModelMatrix * aPosition;

    vec3 lightDirection;
    float nDotL;
    vec3 diffuse;

    for (int i = 0; i < NR_POINT_LIGHTS; i++) {
      if(uPointLight[i].uPointLightEnabled){
        lightDirection = normalize(uPointLight[i].uPointLightPosition - vec3(vertexPosition));
        nDotL = max(dot(lightDirection, normal), 0.0);
        diffuse += uPointLight[i].uPointLightColor * uMaterialDiffuse.rgb * nDotL;
      }
    }

    vec3 ambient = uAmbientLightColor * uMaterialDiffuse.rgb;
    vColor = vec4(ambient + diffuse, uMaterialDiffuse.a);
  } else {
    vColor = uMaterialDiffuse;
  }

  // gl.uniform1i(program.uUseTexture, true);
  if(uUseTexture) {
    vTexcoord = aTexcoord;
  }
}
