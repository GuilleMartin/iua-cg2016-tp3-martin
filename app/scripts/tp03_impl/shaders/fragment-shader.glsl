precision mediump float;

uniform sampler2D uSampler;
uniform bool uUseTexture;

varying vec4 vColor;
varying vec2 vTexcoord;

void main() {
  gl_FragColor = vColor;
}

