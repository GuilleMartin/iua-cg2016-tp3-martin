precision mediump float;

uniform sampler2D uSampler;
uniform bool uUseTexture;

varying vec4 vColor;
varying vec2 vTexcoord;

void main() {
  if (uUseTexture) {
    gl_FragColor = vColor * texture2D(uSampler, vTexcoord);
  } else {
    gl_FragColor = vColor;
  }
}

