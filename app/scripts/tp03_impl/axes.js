define(['./constants',
        './geometry',
        './material',
        './mesh'],
        function(Constants, Geometry, Material, Mesh){

  var cons = Object.create(Constants);
  var colors = cons.colors;

  var Axes = {

    init: function(tx, ty, tz){
      this._tx = tx;
      this._ty = ty;
      this._tz = tz;
    },

    getAxes: function(){
      var axes = [];

      var material;

      //X,Y,Z Axes Meshes:

      var xAxis = Object.create(Geometry);
      xAxis.init();
      xAxis.vertices.push([this._tx, this._ty, this._tz], [this._tx + 10, this._ty, this._tz]);
      xAxis.faces.push([0, 1]);
      xAxis.normals.push([0, 0, 0], [10, 0, 0]);
      xAxis.st.push([1,1]);

      var lineMeshX = Object.create(Mesh);

      material = Object.create(Material);
      material.init('#FF0000', [0, 0, 0], 0, '');

      lineMeshX.init(xAxis, material, 'LINES');
      lineMeshX.useLighting = false;
      lineMeshX.useTexture = false;
      lineMeshX.pickingColor = colors.BLACK;
      axes.push(lineMeshX);

      var yAxis = Object.create(Geometry);
      yAxis.init();
      yAxis.vertices.push([this._tx, this._ty, this._tz], [this._tx, this._ty + 10, this._tz]);
      yAxis.faces.push([0, 1]);
      yAxis.normals.push([0, 0, 0], [0, 10, 0]);
      yAxis.st.push([1,1]);

      var lineMeshY = Object.create(Mesh);

      material = Object.create(Material);
      material.init('#00ff00', [0, 0, 0], 0, '');

      lineMeshY.init(yAxis, material, 'LINES');
      lineMeshY.useLighting = false;
      lineMeshY.useTexture = false;
      lineMeshY.pickingColor = colors.BLACK;
      axes.push(lineMeshY);

      var zAxis = Object.create(Geometry);
      zAxis.init();
      zAxis.vertices.push([this._tx, this._ty, this._tz], [this._tx, this._ty, this._tz + 10]);
      zAxis.faces.push([0, 1]);
      zAxis.normals.push([0, 0, 0], [0, 0, 10]);
      zAxis.st.push([1,1]);

      var lineMeshZ = Object.create(Mesh);

      material = Object.create(Material);
      material.init('#0000ff', [0, 0, 0], 0, '');

      lineMeshZ.init(zAxis, material, 'LINES');
      lineMeshZ.useLighting = false;
      lineMeshZ.useTexture = false;
      lineMeshZ.pickingColor = colors.BLACK;
      axes.push(lineMeshZ);

      return axes;
    }
  };
  return Axes;
});
