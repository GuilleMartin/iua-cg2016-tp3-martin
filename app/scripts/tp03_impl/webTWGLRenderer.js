define(['text!tp03_impl/shaders/vertex-shader.glsl',
        'text!tp03_impl/shaders/fragment-shader-tex.glsl',
        'text!tp03_impl/shaders/picking-vertex-shader.glsl',
        'text!tp03_impl/shaders/picking-fragment-shader.glsl',
        'glMatrix',
        'CuonUtils',
        'utils',
        'twgl',
        'jquery',
        './constants',
        './axes'],
        function(vsSource, fsSource, pvsSource, pfsSource, glMatrix, CuonUtils, utils, TWGL, $, Constants, Axes){


  //var cons = Object.create(Constants);
  //var colors = cons.colors;
  var _pickingCoord = null;
  var _drawingProgramInfo = null;
  var _pickingProgramInfo = null;
  var _pickingFramebufferInfo = null;
  var _sharedUniforms = null;
  var _drawingObjects = [];
  var _canvas = null;
  var _drawMode = null;
  var _gl = null;
  var _readout = new Uint8Array(1 * 1 * 4);
  var _textures = null;
  var _colorPicked = [0, 0, 0, 255];

  function init(canvas, scene, camera, drawMode){
    _canvas = canvas;
    _drawMode = drawMode;

    //Init TWGL
    _gl = TWGL.getWebGLContext(_canvas);
    _drawingProgramInfo = TWGL.createProgramInfo(_gl, [vsSource, fsSource]);
    _pickingProgramInfo = TWGL.createProgramInfo(_gl, [pvsSource, pfsSource]);

    //Set background color
    _gl.clearColor(scene.clearColor[0],
                   scene.clearColor[1],
                   scene.clearColor[2],
                   scene.clearColor[3]);

    //Attachment for picking
    var attachments = [
      {
        format: _gl.RGBA,
        type:   _gl.UNSIGNED_BYTE,
        min:    _gl.LINEAR,
        mag:    _gl.LINEAR,
        wrap:   _gl.CLAMP_TO_EDGE
      },
      {
        format: _gl.DEPTH_COMPONENT16,
      },
    ];

    _pickingFramebufferInfo = TWGL.createFramebufferInfo(_gl, attachments, _canvas.width, _canvas.height);
    _gl.bindFramebuffer(_gl.FRAMEBUFFER, null);

    //Set GL engine params
    _gl.enable(_gl.DEPTH_TEST);
    _gl.depthFunc(_gl.LEQUAL);
    _gl.clearDepth(1.0);
    _gl.enable(_gl.CULL_FACE);
    _gl.cullFace(_gl.BACK);

    for(var i=0; i < scene.meshes.length; i++){

      if(scene.meshes[i].material.map.length > 0){
        var texture = TWGL.createTextures(_gl, {
          image: {
            src: scene.meshes[i].material.map,
            mag: _gl.LINEAR,
            min: _gl.LINEAR,
          }
        });

        scene.meshes[i].material.texture = texture;
      }
    }

    //Stablish a listener for a mouse click event over canvas
    $(_canvas).on('click', function(evt) {
      _pickingCoord = {
        x: evt.offsetX,
        y: _canvas.height - evt.offsetY
      };
    });
  }

  function render(scene, camera, deltaTime){
    var i, j;
    var mesh;

    //Init shared uniforms
    _sharedUniforms = {
        uProjectionMatrix: camera.getProjectionMatrix(),
        uViewMatrix: camera.getViewMatrix(),
        uAmbientLightColor: new Float32Array(scene.ambientLight.color.vec3()),

        'uPointLight[0].uPointLightEnabled': scene.pointLights[0].enabled,
        'uPointLight[0].uPointLightColor': new Float32Array(scene.pointLights[0].color.vec3()),
        'uPointLight[0].uPointLightPosition': new Float32Array(glMatrix.vec3.fromValues(scene.pointLights[0].px,
                                                                                        scene.pointLights[0].py,
                                                                                        scene.pointLights[0].pz)),

        'uPointLight[1].uPointLightEnabled': scene.pointLights[1].enabled,
        'uPointLight[1].uPointLightColor': new Float32Array(scene.pointLights[1].color.vec3()),
        'uPointLight[1].uPointLightPosition': new Float32Array(glMatrix.vec3.fromValues(scene.pointLights[1].px,
                                                                                        scene.pointLights[1].py,
                                                                                        scene.pointLights[1].pz)),

        'uPointLight[2].uPointLightEnabled': scene.pointLights[2].enabled,
        'uPointLight[2].uPointLightColor': new Float32Array(scene.pointLights[2].color.vec3()),
        'uPointLight[2].uPointLightPosition': new Float32Array(glMatrix.vec3.fromValues(scene.pointLights[2].px,
                                                                                        scene.pointLights[2].py,
                                                                                        scene.pointLights[2].pz)),

        'uPointLight[3].uPointLightEnabled': scene.pointLights[3].enabled,
        'uPointLight[3].uPointLightColor': new Float32Array(scene.pointLights[3].color.vec3()),
        'uPointLight[3].uPointLightPosition': new Float32Array(glMatrix.vec3.fromValues(scene.pointLights[3].px,
                                                                                        scene.pointLights[3].py,
                                                                                        scene.pointLights[3].pz)),

        'uPointLight[4].uPointLightEnabled': scene.pointLights[4].enabled,
        'uPointLight[4].uPointLightColor': new Float32Array(scene.pointLights[4].color.vec3()),
        'uPointLight[4].uPointLightPosition': new Float32Array(glMatrix.vec3.fromValues(scene.pointLights[4].px,
                                                                                        scene.pointLights[4].py,
                                                                                        scene.pointLights[4].pz)),

    };

    _gl.pixelStorei(_gl.UNPACK_FLIP_Y_WEBGL, true);


    //Init an array with objects to draw
    _drawingObjects = [];

    //Sets drawing objects
    for(i=0; i < scene.meshes.length; i++){
      mesh = scene.meshes[i];

      addDrawingObjects(mesh);

      if(glMatrix.vec4.equals(mesh.pickingColor, _colorPicked)){

        var ax = Object.create(Axes);
        ax.init(mesh._tx, mesh._ty, mesh._tz);
        var axes = ax.getAxes();

        if(axes !== undefined){
          for(j = 0; j < axes.length; j++){
            addDrawingObjects(axes[j]);
          }
        }
      }
    }

    //handle mouse click event
    if (_pickingCoord) {
      _gl.bindFramebuffer(_gl.FRAMEBUFFER, _pickingFramebufferInfo.framebuffer);
      renderize(_pickingProgramInfo, _gl.TRIANGLES, deltaTime);

      // read one pixel
      _gl.readPixels(_pickingCoord.x, _pickingCoord.y, 1, 1, _gl.RGBA,
                         _gl.UNSIGNED_BYTE, _readout);

      _gl.bindFramebuffer(_gl.FRAMEBUFFER, null);

      if(!glMatrix.vec4.equals(_readout, [0, 0, 0, 255])){
        if(glMatrix.vec4.equals(_colorPicked, _readout)){
          _colorPicked = [0, 0, 0, 255];
        }else{
          console.log('picked color', _readout);
          _colorPicked = _readout;
        }

        _readout = new Uint8Array(1 * 1 * 4);

      }else{
        _colorPicked = [0, 0, 0, 255];
      }

      _pickingCoord = null;
    }

    renderize(_drawingProgramInfo, deltaTime);
  }

  function renderize(programInfo, deltaTime){
    _gl.bindFramebuffer(_gl.FRAMEBUFFER, null);
    TWGL.resizeCanvasToDisplaySize(_gl.canvas);
    _gl.viewport(0, 0, _gl.canvas.width, _gl.canvas.height);
    _gl.clear(_gl.COLOR_BUFFER_BIT  | _gl.DEPTH_BUFFER_BIT);

    for (var i = 0; i < _drawingObjects.length; i++) {

      /*
      glMatrix.mat4.rotate(
        _drawingObjects[i].localUniforms.uModelMatrix,
        _drawingObjects[i].localUniforms.uModelMatrix,
        deltaTime * 0.001,
        [1, 1, 0]
      );

      glMatrix.mat4.invert(
        _drawingObjects[i].localUniforms.uModelMatrix,
        _drawingObjects[i].localUniforms.uModelMatrix
      );

      glMatrix.mat4.transpose(
        _drawingObjects[i].localUniforms.uModelMatrix,
        _drawingObjects[i].localUniforms.uModelMatrix
      );*/

      /*if(i === 51){
      glMatrix.mat4.rotateY(
        _drawingObjects[i].localUniforms.uModelMatrix,
        _drawingObjects[i].localUniforms.uModelMatrix,
        deltaTime * 0.001
      );
      }*/

      if(_drawingObjects[i].visible){
        _gl.useProgram(programInfo.program);
        TWGL.setBuffersAndAttributes(_gl, programInfo, _drawingObjects[i].bufferInfo);
        TWGL.setUniforms(programInfo, _sharedUniforms, _drawingObjects[i].localUniforms);

        if(_drawingObjects[i].mode === 'TRIANGLES'){
          var drawMode;

          if(_drawMode === 1){
            drawMode = _gl.TRIANGLES;
          }else{
            drawMode = _gl.LINE_LOOP;
          }

          TWGL.drawBufferInfo(_gl, drawMode, _drawingObjects[i].bufferInfo);
        }else if (_drawingObjects[i].mode === 'LINES'){
          TWGL.drawBufferInfo(_gl, _gl.LINES, _drawingObjects[i].bufferInfo);
        }
      }

      _gl.bindFramebuffer(_gl.FRAMEBUFFER, null);
    }
  }

  function addDrawingObjects(mesh){
    //normalMatrix
    mesh.normalMatrix = glMatrix.mat4.create();
    glMatrix.mat4.invert(mesh.normalMatrix, mesh.getModelMatrix());
    glMatrix.mat4.transpose(mesh.normalMatrix, mesh.normalMatrix);

    //set mesh's local uniform
    var localUniforms = {
      uModelMatrix: mesh.getModelMatrix(),
      uNormalMatrix: mesh.normalMatrix,
      uUseLighting: mesh.useLighting,
      uUseTexture: mesh.useTexture,
      uPickingColor: new Float32Array(mesh.pickingColor),
      uMaterialDiffuse: new Float32Array(mesh.material.diffuse.vec4()),
      uMaterialSpecular: new Float32Array(mesh.material.specular),
      uMaterialShininess: mesh.material.shininess
    };

    var triangleVerticesInfo = {
      aPosition: new Float32Array(utils.flatten(mesh.geometry.vertices)),
      indices: utils.flatten(mesh.geometry.faces),
      aNormal: new Float32Array(utils.flatten(mesh.geometry.normals))
    };

    if(mesh.useTexture){
      localUniforms.uSampler = mesh.material.texture.image;
      triangleVerticesInfo.aTexcoord = utils.flatten(mesh.geometry.st);
    }

    var bufferInfo = TWGL.createBufferInfoFromArrays(_gl, triangleVerticesInfo);

    _drawingObjects.push({
      bufferInfo: bufferInfo,
      localUniforms: localUniforms,
      mode: mesh.mode,
      visible: mesh.visible
    });
  }

  return {
    render: render,
    init: init,
    get drawMode() {
      return _drawMode;
    },
    set drawMode(value){
      _drawMode = value;
    },
  };
});
