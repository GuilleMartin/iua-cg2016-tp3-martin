define(['./color/style'], function(Style){

  var Light = {

    init: function(color){
      this.hexColor = color;
      this.color = Object.create(Style);
      this.color.init(this.hexColor);
    }
  };

  return Light;
});
