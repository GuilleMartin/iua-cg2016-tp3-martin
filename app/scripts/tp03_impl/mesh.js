define(['./geometry', 'glMatrix'], function(Geometry, glMatrix){

  var mat4 = glMatrix.mat4;

  var Mesh = {

    init: function(geometry, material, mode){

      this._id = null;
      this.geometry = geometry;
      this._material = material;
      this.mode = mode;
      this._modelMatrix = mat4.create();
      this._normalMatrix = mat4.create();
      this._tx = 0;
      this._ty = 0;
      this._tz = 0;
      this._rx = 0;
      this._ry = 0;
      this._rz = 0;
      this._sx = 1;
      this._sy = 1;
      this._sz = 1;
      this._visible = true;
    },

    get id() {
      return this._id;
    },
    set id(value) {
      this._id = value;
    },
    get name() {
      return this._name;
    },
    set name(value) {
      this._name = value;
    },
    get material() {
      return this._material;
    },
    set material(value) {
      this._material = value;
    },
    get tx() {
      return this._tx;
    },
    set tx(value) {
      this._tx = value;
    },

    get ty() {
      return this._ty;
    },
    set ty(value) {
      this._ty = value;
    },

    get tz() {
      return this._tz;
    },
    set tz(value) {
      this._tz = value;
    },

    get rx() {
      return this._rx;
    },
    set rx(value) {
      this._rx = value;
    },

    get ry() {
      return this._ry;
    },
    set ry(value) {
      this._ry = value;
    },

    get rz() {
      return this._rz;
    },
    set rz(value) {
      this._rz = value;
    },

    get sx() {
      return this._sx;
    },
    set sx(value) {
      this._sx = value;
    },

    get sy() {
      return this._sy;
    },
    set sy(value) {
      this._sy = value;
    },

    get sz() {
      return this._sz;
    },
    set sz(value) {
      this._sz = value;
    },

    get modelMatrix() {
      return this._modelMatrix;
    },
    set modelMatrix(value) {
      this._modelMatrix = value;
    },

    get normalMatrix() {
      return this._normalMatrix;
    },
    set normalMatrix(value) {
      this._normalMatrix = value;
    },

    get useLighting() {
      return this._useLighting;
    },
    set useLighting(value) {
      this._useLighting = value;
    },
    get useTexture() {
      return this._useTexture;
    },
    set useTexture(value) {
      this._useTexture = value;
    },
    get pickingColor() {
      return this._pickingColor;
    },
    set pickingColor(value) {
      this._pickingColor = value;
    },
    get visible() {
      return this._visible;
    },
    set visible(value) {
      this._visible = value;
    },

    getModelMatrix: function(){

      //Translate mesh
      mat4.translate(this._modelMatrix, mat4.identity(this._modelMatrix), [this._tx, this._ty, this._tz]);

      //Scale mesh
      mat4.scale(this._modelMatrix, this._modelMatrix, [this._sx, this._sy, this._sz]);

      //Rotate mesh
      mat4.rotateX(this._modelMatrix, this._modelMatrix, this._rx);
      mat4.rotateY(this._modelMatrix, this._modelMatrix, this._ry);
      mat4.rotateZ(this._modelMatrix, this._modelMatrix, this._rz);

      return this._modelMatrix;
    },

    getNormalMatrix: function(){

      return this._normalMatrix;
    }
  };

  return Mesh;
});
