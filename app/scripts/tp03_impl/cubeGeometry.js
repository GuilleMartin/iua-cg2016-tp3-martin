define(['./geometry'], function(Geometry){

  var CubeGeometry = Object.create(Geometry);

  CubeGeometry.init = function(size){

    Geometry.init.call(this);
    this._size = size;

    var halfSize = size / 2;

    this._vertices.push(
        [ halfSize, halfSize, halfSize], [-halfSize, halfSize, halfSize], [-halfSize,-halfSize, halfSize], [ halfSize,-halfSize, halfSize], // Front face
        [ halfSize, halfSize, halfSize], [ halfSize,-halfSize, halfSize], [ halfSize,-halfSize,-halfSize], [ halfSize, halfSize,-halfSize], // Front face
        [ halfSize, halfSize, halfSize], [ halfSize, halfSize,-halfSize], [-halfSize, halfSize,-halfSize], [-halfSize, halfSize, halfSize], // Front face
        [-halfSize, halfSize, halfSize], [-halfSize, halfSize,-halfSize], [-halfSize,-halfSize,-halfSize], [-halfSize,-halfSize, halfSize], // Front face
        [-halfSize,-halfSize,-halfSize], [ halfSize,-halfSize,-halfSize], [ halfSize,-halfSize, halfSize], [-halfSize,-halfSize, halfSize], // Front face
        [ halfSize,-halfSize,-halfSize], [-halfSize,-halfSize,-halfSize], [-halfSize, halfSize,-halfSize], [ halfSize, halfSize,-halfSize]  // Front face
        );

    this._faces.push(
        [ 0,  1,  2], [ 0,  2,  3], // Front face
        [ 4,  5,  6], [ 4,  6,  7], // Back face
        [ 8,  9, 10], [ 8, 10, 11], // Top face
        [12, 13, 14], [12, 14, 15], // Bottom face
        [16, 17, 18], [16, 18, 19], // Right face
        [20, 21, 22], [20, 22, 23]  // Left face

    );

    this._normals.push(
      [ 0.0, 0.0, 1.0], [ 0.0, 0.0, 1.0], [ 0.0, 0.0, 1.0], [ 0.0, 0.0, 1.0], //front
      [ 1.0, 0.0, 0.0], [ 1.0, 0.0, 0.0], [ 1.0, 0.0, 0.0], [ 1.0, 0.0, 0.0], //right
      [ 0.0, 1.0, 0.0], [ 0.0, 1.0, 0.0], [ 0.0, 1.0, 0.0], [ 0.0, 1.0, 0.0], //up
      [-1.0, 0.0, 0.0], [-1.0, 0.0, 0.0], [-1.0, 0.0, 0.0], [-1.0, 0.0, 0.0], //left
      [ 0.0,-1.0, 0.0], [ 0.0,-1.0, 0.0], [ 0.0,-1.0, 0.0], [ 0.0,-1.0, 0.0], //down
      [ 0.0, 0.0,-1.0], [ 0.0, 0.0,-1.0], [ 0.0, 0.0,-1.0], [ 0.0, 0.0,-1.0] //back
    );

    this._st.push(
      [1.0, 1.0], [0.0, 1.0], [0.0, 0.0], [1.0, 0.0],
      [1.0, 1.0], [0.0, 1.0], [0.0, 0.0], [1.0, 0.0],
      [1.0, 1.0], [0.0, 1.0], [0.0, 0.0], [1.0, 0.0],
      [1.0, 1.0], [0.0, 1.0], [0.0, 0.0], [1.0, 0.0],
      [1.0, 1.0], [0.0, 1.0], [0.0, 0.0], [1.0, 0.0],
      [1.0, 1.0], [0.0, 1.0], [0.0, 0.0], [1.0, 0.0]
    );
  };

  Object.defineProperty(CubeGeometry, 'size', {
    get: function() {
      return this._size;
    },
    set: function(value) {
      this._size = value;
    }
  });

  Object.defineProperty(CubeGeometry, 'vertices', {
    get: function() {
      return this._vertices;
    },
    set: function(value) {
      this._vertices = value;
    }
  });

  Object.defineProperty(CubeGeometry, 'faces', {
    get: function() {
      return this._faces;
    },
    set: function(value) {
      this._faces = value;
    }
  });

  Object.defineProperty(CubeGeometry, 'normals', {
    get: function() {
      return this._normals;
    },
    set: function(value) {
      this._normals = value;
    }
  });

  Object.defineProperty(CubeGeometry, 'st', {
    get: function() {
      return this._st;
    },
    set: function(value) {
      this._st = value;
    }
  });

  return CubeGeometry;
});
