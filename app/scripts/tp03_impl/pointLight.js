define(['./light'], function(Light){

  var PointLight = Object.create(Light);

  PointLight.init = function(px, py, pz, color, enabled){

    Light.init.call(this, color);
    this._px = px;
    this._py = py;
    this._pz = pz;
    this._enabled = enabled;
    this._pointLightMeshId = null;
    this._diffuse = color;
  };

  Object.defineProperty(PointLight, 'position', {
    get: function() {
      return this._position;
    },
    set: function(value) {
      this._position = value;
    }
  });

  Object.defineProperty(PointLight, 'diffuse', {
    get: function() {
      return this._diffuse;
    },
    set: function(value) {
      this._diffuse = value;
      Light.init.call(this, value);
    }
  });

  Object.defineProperty(PointLight, 'px', {
    get: function() {
      return this._px;
    },
    set: function(value) {
      this._px = value;
    }
  });

  Object.defineProperty(PointLight, 'py', {
    get: function() {
      return this._py;
    },
    set: function(value) {
      this._py = value;
    }
  });

  Object.defineProperty(PointLight, 'pz', {
    get: function() {
      return this._pz;
    },
    set: function(value) {
      this._pz = value;
    }
  });

  Object.defineProperty(PointLight, 'enabled', {
    get: function() {
      return this._enabled;
    },
    set: function(value) {
      this._enabled = value;
    }
  });

  Object.defineProperty(PointLight, 'pointLightMeshId', {
    get: function() {
      return this._pointLightMeshId;
    },
    set: function(value) {
      this._pointLightMeshId = value;
    }
  });

  PointLight.position = function(){
    var pos;
    pos.push([this._px, this._py, this._pz]);
    return pos;
  };

  return PointLight;
});
