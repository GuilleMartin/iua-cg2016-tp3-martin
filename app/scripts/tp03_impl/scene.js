define(function(){

  var Scene = {

    init: function(clearColor){
      this._lastMeshId = null;
      this.clearColor = clearColor;
      this.meshes = [];
      this._ambientLight = null;
      this._pointLights = [];
    },

    addMesh: function(mesh){
      if(this._lastMeshId === null){
        this._lastMeshId = 0;
      }else{
        this._lastMeshId++;
      }

      mesh.id = this._lastMeshId;
      this.meshes.push(mesh);
    },

    get ambientLight() {
      return this._ambientLight;
    },
    set ambientLight(value) {
      this._ambientLight = value;
    },

    get pointLights() {
      return this._pointLights;
    },
    set pointLights(value) {
      this._pointLights = value;
    },

    get lastMeshId() {
      return this._lastMeshId;
    },
    set lastMeshId(value) {
      this._lastMeshId = value;
    }
  };

  return Scene;
});
