define(['./geometry'], function(Geometry){

  var RegularConvexPolygonGeometry = Object.create(Geometry);

  RegularConvexPolygonGeometry.init = function(edges){

    Geometry.init.call(this);
    this._edges = edges;

    var angle;
    this.vertices.push([0, 0, 0]);  //Vertex[0]

    for (var i=0; i < edges + 1; i++){

      angle = (i * 2 * Math.PI) / edges;
      this.vertices.push([Math.cos(angle), Math.sin(angle), 0]); //(x,y,z)
      this.faces.push([0, i, i+1]);  // El primero es siempre Vertex[0]:0,0,0
    }
  };

  Object.defineProperty(RegularConvexPolygonGeometry, 'edges', {
    get: function() {
      return this._edges;
    },
    set: function(value) {
      this._edges = value;
    }
  });

  return RegularConvexPolygonGeometry;
});
