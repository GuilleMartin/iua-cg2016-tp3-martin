define(['text!tp03_impl/shaders/vertex-shader.glsl',
        'text!tp03_impl/shaders/fragment-shader.glsl',
        'glMatrix',
        'CuonUtils',
        'utils'],
        function(vsSource, fsSource, glMatrix, CuonUtils, utils){

function initBuffer(gl, mesh){
    if(mesh._webgl){
      return;
    }

    mesh._webgl = {};

    // Create Vertex Buffer Object
    var VBOData = mesh._webgl.vboData = new Float32Array(utils.flatten(mesh.geometry.vertices));
    var VBO = mesh._webgl.vbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, VBO);
    gl.bufferData(gl.ARRAY_BUFFER, VBOData, gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // Create Index Buffer Object
    var IBOData = mesh._webgl.iboData = new Uint16Array(utils.flatten(mesh.geometry.faces));
    var IBO = mesh._webgl.ibo = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, IBO);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, IBOData, gl.STATIC_DRAW);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    // Normals Buffer Object
    var NBOData = mesh._webgl.nboData = new Float32Array(utils.flatten(mesh.geometry.normals));
    var NBO = mesh._webgl.nbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, NBO);
    gl.bufferData(gl.ARRAY_BUFFER, NBOData, gl.STATIC_DRAW);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
    NBO.count = NBOData.length;
    NBO.type = gl.FLOAT;


    // Texture Buffer Object
    var TBOData = mesh._webgl.tboData = new Float32Array(utils.flatten(mesh.geometry.st));
    var TBO = mesh._webgl.tbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, TBO);
    gl.bufferData(gl.ARRAY_BUFFER, TBOData, gl.STATIC_DRAW);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
    TBO.count = TBOData.length;
    TBO.type = gl.FLOAT;
  }

  function initTextures(gl,mesh){
    if(mesh._webgl.texture){
      return;
    }

    mesh._webgl.texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, mesh._webgl.texture);
    gl.texImage2D(gl.TEXTURE_2D,0,gl.RGB,1,1,0,gl.RGB,gl.UNSIGNED_BYTE,null);

    var image = new Image();
    image.src = mesh.material.map;
    image.onload = (function() { handleTextureLoaded(gl,image,mesh._webgl.texture); });
    return true;
  }

  function handleTextureLoaded(gl,image,texture){

    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);
    //gl.uniform1i(gl.uSampler, 0);
  }

  var WebGLRenderer = {

    init: function(canvas, drawMode){
      this.canvas = canvas;
      this._drawMode = drawMode;
    },

    get drawMode() {
      return this._drawMode;
    },
    set drawMode(value){
      this._drawMode = value;
    },

    render: function(scene, camera){

      var gl = CuonUtils.getWebGLContext(this.canvas);

      if (!gl) {
        console.log('Failed to get the rendering context for WebGL');
        return;
      }

      // Initialize shaders
      if (!CuonUtils.initShaders(gl, vsSource, fsSource)) {
        console.log('Failed to intialize shaders.');
        return;
      }

      var program = gl.program;

      // Specify the color for clearing <canvas>
      gl.clearColor(scene.clearColor[0],
                    scene.clearColor[1],
                    scene.clearColor[2],
                    scene.clearColor[3]);

      gl.enable(gl.DEPTH_TEST);
      gl.enable(gl.CULL_FACE);

      // Clear <canvas>
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

      // Cache attribute/uniform location
      var aPosition = gl.getAttribLocation(program, 'aPosition');
      gl.enableVertexAttribArray(aPosition);

      var aNormal = gl.getAttribLocation(program, 'aNormal');
      gl.enableVertexAttribArray(aNormal);

      var aTexcoord = gl.getAttribLocation(program, 'aTexcoord');


      var uColor = gl.getUniformLocation(program, 'uColor');
      var uModelMatrix = gl.getUniformLocation(program, 'uModelMatrix');
      var uProjectionMatrix = gl.getUniformLocation(program, 'uProjectionMatrix');
      var uViewMatrix = gl.getUniformLocation(program, 'uViewMatrix');
      var uNormalMatrix = gl.getUniformLocation(program, 'uNormalMatrix');
      var uAmbientLightColor = gl.getUniformLocation(program, 'uAmbientLightColor');
      var uPointLightColor = gl.getUniformLocation(program, 'uPointLightColor' );
      var uPointLightPosition = gl.getUniformLocation(program, 'uPointLightPosition');
      var uUseLighting = gl.getUniformLocation(program, 'uUseLighting');


      var uUseTexture = gl.getUniformLocation(program, 'uUseTexture' ) ;
      var uSampler = gl.getUniformLocation(program, 'uSampler');
      gl.uSampler = uSampler;

      gl.uniformMatrix4fv(uProjectionMatrix, false, camera.getProjectionMatrix());
      gl.uniformMatrix4fv(uViewMatrix, false, camera.getViewMatrix());

      var ambientLightColor = glMatrix.vec3.create();
      ambientLightColor = scene.ambientLight.color;
      gl.uniform3fv(uAmbientLightColor, new Float32Array(ambientLightColor.vec3()));

      var pointLightColors = glMatrix.vec3.create();
      pointLightColors = scene.pointLight.color;
      gl.uniform3fv(uPointLightColor, new Float32Array(pointLightColors.vec3()));

      var pointLightPosition = glMatrix.vec3.fromValues(scene.pointLight.px, scene.pointLight.py, scene.pointLight.pz);

      gl.uniform3fv(uPointLightPosition, new Float32Array(pointLightPosition));

      for(var i=0; i < scene.meshes.length; i++){
        var mesh = scene.meshes[i];

        gl.useProgram(program);

        initBuffer(gl, mesh);

        gl.bindBuffer(gl.ARRAY_BUFFER, mesh._webgl.vbo);
        // Bind Buffer to a shader attribute
        gl.vertexAttribPointer(
          aPosition, 3, gl.FLOAT, false,
          0, 0
        );
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        //Normals
        gl.bindBuffer(gl.ARRAY_BUFFER, mesh._webgl.nbo);
        gl.vertexAttribPointer(aNormal, 3, mesh._webgl.nbo.type, false ,0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        //Textures
        gl.uniform1i(uUseTexture, mesh.useTexture);

        if(mesh.useTexture){
          gl.bindBuffer(gl.ARRAY_BUFFER, mesh._webgl.tbo);
          gl.vertexAttribPointer(aTexcoord, 2, mesh._webgl.tbo.type, false, 0, 0);
          gl.bindBuffer(gl.ARRAY_BUFFER, null);
          gl.bindTexture(gl.TEXTURE_2D,mesh._webgl.texture);
          gl.uniform1i(gl.uSampler, 0);
          gl.enableVertexAttribArray(aTexcoord);
        }
        //gl.enableVertexAttribArray(aTexCoord);
        gl.uniform1i(uUseLighting, mesh.useLighting);

        var Color = new Float32Array(mesh.material.color);
        gl.uniform4fv(uColor, Color);

        //Translacción, escalación, rotación
        gl.uniformMatrix4fv(uModelMatrix, false, mesh.getModelMatrix());

        //normalMatrix
        mesh.normalMatrix = glMatrix.mat4.create();
        glMatrix.mat4.invert(mesh.normalMatrix, mesh.getModelMatrix());
        glMatrix.mat4.transpose(mesh.normalMatrix, mesh.normalMatrix);
        gl.uniformMatrix4fv(uNormalMatrix, false, mesh.normalMatrix);


        gl.uniform1i(uUseTexture, mesh.useTexture);
        if(mesh.useTexture){
          initTextures(gl,mesh);
        }
        //gl.uniform1i(gl.uSampler, 0);

        // Draw Mesh
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, mesh._webgl.ibo);

        if(mesh.mode === 'TRIANGLES'){
          var drawMode;

          if(this._drawMode == 1){
            drawMode = gl.TRIANGLES;
          }else{
            drawMode = gl.LINE_LOOP;
          }

          gl.drawElements(drawMode, mesh._webgl.iboData.length, gl.UNSIGNED_SHORT, 0);
        }else if (mesh.mode === 'LINES'){
          gl.drawElements(gl.LINES, 2, gl.UNSIGNED_SHORT, 0);
        }

        //gl.drawElements(gl.TRIANGLES, mesh._webgl.iboData.length, gl.UNSIGNED_SHORT, 0);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

      }
    }
  };


  return WebGLRenderer;
});
