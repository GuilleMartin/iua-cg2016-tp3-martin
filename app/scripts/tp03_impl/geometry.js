define(function(){

  var Geometry = {

    init: function(){
      this._vertices = [];
      this._faces = [];
      this._normals = [];
      this._st = [];
    },

    get vertices() {
      return this._vertices;
    },
    set vertices(value) {
      this._vertices = value;
    },
    get faces() {
      return this._faces;
    },
    set faces(value) {
      this._faces = value;
    },
    get normals() {
      return this._normals;
    },
    set normals(value) {
      this._normals = value;
    },
    get st() {
      return this._st;
    },
    set st(value) {
      this._st = value;
    }
  };

  return Geometry;
});
