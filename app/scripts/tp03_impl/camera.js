define(['glMatrix'], function(glMatrix) {

  var mat4 = glMatrix.mat4;

  var Camera = {

    init: function(){

      this._viewMatrix = mat4.create();
      this._projectionMatrix = mat4.create();

      this._eyeX = 5;
      this._eyeY = 5;
      this._eyeZ = 5;
      this._centerX = 0;
      this._centerY = 0;
      this._centerZ = 0;
      this._upX = 0;
      this._upY = 1;
      this._upZ = 0;

    },

    /*set viewMatrix(value){
      this._viewMatrix = value;
    },

    set vrojectionMatrix(value){
      this._projectionMatrix = value;
    },*/

    get eyeX() {
      return this._eyeX;
    },

    set eyeX(value){
      this._eyeX = value;
    },

    get eyeY() {
      return this._eyeY;
    },

    set eyeY(value){
      this._eyeY = value;
    },

    get eyeZ() {
      return this._eyeZ;
    },

    set eyeZ(value){
      this._eyeZ = value;
    },

    get centerX() {
      return this._centerX;
    },

    set centerX(value){
      this._centerX = value;
    },

    get centerY() {
      return this._centerY;
    },

    set centerY(value){
      this._centerY = value;
    },

    get centerZ() {
      return this._centerZ;
    },

    set centerZ(value){
      this._centerZ = value;
    },

    get upX() {
      return this._upX;
    },

    set upX(value){
      this._upX = value;
    },

    get upY() {
      return this._upY;
    },

    set upY(value){
      this._upY = value;
    },

    get upZ() {
      return this._upZ;
    },

    set upZ(value){
      this._upZ = value;
    },

    getViewMatrix: function(){

      mat4.lookAt(this._viewMatrix,
                  [this._eyeX, this._eyeY, this._eyeZ],
                  [this._centerX, this._centerY, this._centerZ],
                  [this._upX, this._upY, this._upZ]);

      return this._viewMatrix;
    },

    getProjectionMatrix: function(){
      return this._projectionMatrix;
    }
  };

  return Camera;
});

